# ARGO

"Argo Workflows is an open source container-native workflow engine for orchestrating parallel jobs on Kubernetes. Argo Workflows is implemented as a Kubernetes CRD."
## Get started
Check [here](https://github.com/argoproj/argo-workflows/blob/master/docs/quick-start.md) to get started with Argo and deploy it on your machine.

In my case, these three commands are the most important
``` bash
# Create the argo namespace
kubectl create ns argo

# apply the commonly used components
kubectl apply -n argo -f https://raw.githubusercontent.com/argoproj/argo-workflows/stable/manifests/quick-start-postgres.yaml

# Open a port forwarding to access the argo namespace
kubectl -n argo port-forward deployment/argo-server 2746:2746
```

## Persistent volumes in Argo
Check [here](./volumes/README.md) for mounting persistent volumes in Argo.

## GDAL within Argo
Check [here](./gdal/README.md) for some examples on how to run a GDAL-based workflow in Argo.
