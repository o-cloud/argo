# GDAL workflow in Argo

This is a small walkthrough on how to run a how to get a GDAL workflow running within Argo.
You first need to be sure that you have argo installed on your cluster.
Run the install script in case argo is not installed:
```bash
sh argo_install.sh
```

## Get the `PersistentVolume` Ready
If you want to set a local `PersistentVolume` you first need to set the location
in the `pv-volume_argo_ns.yaml` file, under the `hostPath` section.

Then run the following
```bash
kubectl apply -f pv-volume_argo_ns.yaml
kubectl apply -f pv-claim_argo_ns.yaml
```

## Start the workflow
We present here two workflows. They do exactly the same thing, but not in the same way. 
The `gdal_raster_calculation.yaml` workflow does the following:
* convert `image.tif` to `image.JPEG`
* convert `image.tif` to `image.JPEG` and change the resolution
* convert `image.tif` to `image_new_proj.tif` and change the projection 

All the steps are executed one after the other, no matter the result of the previous step. 

The `gdal_dag.yaml` on the other hand, is a workflow based on DAG. The second and third processes are base on the conditions that the previous processes are successful.

You can start the differents workflows with the following commands:
```bash
# To start the standard workflow
argo submit -n argo --watch gdal_raster_calculation.yaml

# To start the DAG-based workflow
argo submit -n argo --watch gdal_dag.yaml
```

