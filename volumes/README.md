# Persistent volumes in Argo
This is a short description on how to mount persistent volume in Argo, accessible 
from the minikube virtual machine.

First make sure that minikube and argo are properly installed on your machine. 

## Create a `PersistentVolume` and a `PersistentVolumeClaim`
Let's first create a `PersistentVolume` that will be mounted in the pod. 
The script `pv-volume_argo_ns.yaml` will create a `PersistentVolume` in the `argo` namespace, 
locate in the `/mnt/data` folder on the host machine. 
You can create the `PersistentVolume` by running
```bash
kubectl -f apply pv-volume_argo_ns.yaml
```
You can check if the `PersistentVolume` has successfully been create by running
``` bash
kubectl get pv | grep task-pv-volume
```

You should see in the output the following line
```
task-pv-volume   10Gi       RWO            Retain           Released   argo/task-pv-claim   manual                  150m
```
meaning that the `PersistentVolume`has been created.

You can now create the `PersistentVolumeClaim`. 
To do so, you can run
``` bash
kubectl apply -f pv-claim_argo_ns.yaml
```

You can check if the `PersistentVolumeClaim` has successfully been create by running
``` bash
kubectl get pvc | grep task-pv-claim
```

You should see in the output the following line
```
task-pv-claim   Bound    task-pv-volume   10Gi       RWO            manual         4s
```
We can see that the `PersistentVolumeClaim` named `task-pv-claim` has been created and bound to the `PersistentVolume`
`task-pv-volume`.

## Retrieve `PersistentVolume` from within Argo

The `persistent-volume-argo.yaml` file contains the workflow that requires the `PersistentVolume`.
The following lines will claim the `PersistentVolume`.
``` yaml
    volumes:
      - name: task-pv-storage
        persistentVolumeClaim:
          claimName: task-pv-claim
```

The container then has to be instructed to use the `PersistentVolume`set:
```yaml
      volumeMounts:
        - mountPath: "/usr/share/"
          name: task-pv-storage
```

The workflow can then be started
```bash
argo submit -n argo --watch persistent-volume-argo.yaml
```

## SSH to minikube
Minikubes create a virtual machine in which the workflow is run. To retrieve the output of the 
`persistent_volume_argo.yaml` workflow, you need to ssh into the minikube machine
```bash
minikube ssh
```
and retrieve the output
```bash 
$ cat /mnt/data/hello_world.txt
hello world$
```

## Note
All the above is valid for minikube. In this case, the `PersistentVolume` is created within the minikube virtual machine. 
Youu can also use Docker Desktop. This way, you can create the `PersistentVolume` directly on your machine and access it from within argo workflow. 
The process is exactly the same.
